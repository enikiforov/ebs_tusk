import re
from datetime import datetime, timedelta

from products.conf import app_conf


def dates_validation(start, end):
    try:
        start_date = datetime.strptime(start, "%Y-%m-%d").date()
    except:
        return 'start_date must have YYYY-MM-DD format'

    min_start_date = datetime.strptime(app_conf['min_start_date'], "%Y-%m-%d").date()
    if min_start_date > start_date:
        return 'start_date must be > ' + app_conf['min_start_date']

    if end == '':
        return True

    try:
        end_date = datetime.strptime(end, "%Y-%m-%d").date()
    except:
        return 'end_date must have YYYY-MM-DD format'

    max_end_date = datetime.strptime(app_conf['max_end_date'], "%Y-%m-%d").date()
    if max_end_date < end_date:
        return 'end_date must be < ' + app_conf['max_end_date']

    if start_date >= end_date:
        return 'start_date must be < end_date'

    return True


def price_validation(price):
    try:
        price = float(price)
    except:
        return 'price must be float'

    if price <= 0:
        return 'price must be > 0'

    return True


def has_empty_fields_validation(required_params, request):
    # check if all field not empty
    empty_fields = []
    for name in required_params:
        if name not in request.POST.keys() or request.POST.get(name) == '':
            empty_fields.append(name + ' cant be empty')

    if len(empty_fields) > 0:
        return empty_fields
    else:
        return False


def string_format_validation(value):
    if not (bool(re.match("^[A-Za-z0-9_-]*$", value))):
        return 'product_type must be A-Z,a-z,0-9,_ format'
    return True


def change_price_date_line(dates_dict, price, interval_start_date, interval_end_date):
    # fill in interval with data
    delta = interval_end_date - interval_start_date
    for i in range(delta.days):
        date = interval_start_date + timedelta(days=i)
        day_key = str(date)
        dates_dict[day_key] = price


def prepare_data(dates_dict, aver_weekly, aver_monthly, dates_interval):
    new_data = []
    dates = []
    for date in dates_dict:
        dates.append(date)
        if date >= dates_interval['start_date'] and date <= dates_interval['end_date']:
            new_data.append({
                'date': date,
                'price': dates_dict[date],
                'average_weekly': aver_weekly[date],
                'average_monthly': aver_monthly[date],
            })

    # fill in empty dates of data interval with -1 value
    start_date = datetime.strptime(new_data[0]['date'], "%Y-%m-%d").date()
    end_date = datetime.strptime(new_data[len(new_data) - 1]['date'], "%Y-%m-%d").date()
    delta = end_date - start_date
    fill_list = []

    for i in range(delta.days):
        date = start_date + timedelta(days=i)
        if str(date) not in dates:
            fill_list.append({
                'date': str(date),
                'price': -1,
                'average_weekly': -1,
                'average_monthly': -1,
            })
    new_data = sorted(new_data + fill_list, key=lambda k: k['date'])

    return new_data


def average_weekly(dates_dict):
    weeks_dict = {}

    # mark days with weeks label
    for date in dates_dict:

        price = dates_dict[date]
        week_key = datetime.strptime(date, "%Y-%m-%d").date().isocalendar()[1]  # number of week in year

        if week_key not in weeks_dict:
            weeks_dict[week_key] = []
            weeks_dict[week_key].append({
                'day': date,
                'price': price
            })
        else:
            weeks_dict[week_key].append({
                'day': date,
                'price': price
            })

    # calc average
    weekly_average_dict = {}
    for key in weeks_dict:
        week = weeks_dict[key]
        s = sum(d['price'] for d in week) / len(week)
        for day in week:
            weekly_average_dict[day['day']] = s

    return weekly_average_dict


def average_monthly(dates_dict):
    months_dict = {}
    for date in dates_dict:

        price = dates_dict[date]
        month_key = datetime.strptime(date, "%Y-%m-%d").date().month  # number of months in year

        # mark days with months label
        if month_key not in months_dict:
            months_dict[month_key] = []
            months_dict[month_key].append({
                'day': date,
                'price': price
            })
        else:
            months_dict[month_key].append({
                'day': date,
                'price': price
            })

    # calc average
    monthly_average_dict = {}
    for key in months_dict:
        month = months_dict[key]
        s = sum(d['price'] for d in month) / len(month)
        for day in month:
            monthly_average_dict[day['day']] = s

    return monthly_average_dict
