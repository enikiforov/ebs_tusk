from django.contrib import admin

from products.models import Products, ProductChangeLog, ProductTypeChangeLog


class ProductsAdmin(admin.ModelAdmin):
    list_display = ('name', 'product_type', 'price','created')


class ProductChangeLogAdmn(admin.ModelAdmin):
    list_display = ('product_name', 'price', 'start_date', 'end_date')

class ProductChangeTypeLogAdmn(admin.ModelAdmin):
    list_display = ('product_type', 'price', 'start_date', 'end_date')


admin.site.register(Products, ProductsAdmin)
admin.site.register(ProductChangeLog, ProductChangeLogAdmn)
admin.site.register(ProductTypeChangeLog, ProductChangeTypeLogAdmn)
