from datetime import datetime

from django.db import models


class Products(models.Model):
    name = models.CharField(unique=True,max_length=30)
    product_type = models.CharField(max_length=30)
    price = models.FloatField(max_length=30)
    created = models.DateField(max_length=30)


class ProductChangeLog(models.Model):
    product_name = models.CharField(max_length=30)
    price = models.FloatField(max_length=30)
    start_date = models.DateField(max_length=30)
    end_date = models.DateField(max_length=30)


class ProductTypeChangeLog(models.Model):
    product_type = models.CharField(max_length=30)
    price = models.FloatField(max_length=30)
    start_date = models.DateField(max_length=30)
    end_date = models.DateField(max_length=30)
