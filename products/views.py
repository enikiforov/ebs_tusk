import json
from datetime import datetime
from itertools import chain

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from products.conf import app_conf
from products.models import Products, ProductTypeChangeLog, ProductChangeLog
from products.utils import has_empty_fields_validation, price_validation, dates_validation, string_format_validation, \
    change_price_date_line, prepare_data, average_weekly, average_monthly


def index(request):
    return render(request, 'index.html')


def insert_product(request):
    if request.method == "POST":

        errors = []

        has_empty_fields = has_empty_fields_validation(required_params=['name', 'product_type', 'price'],
                                                       request=request)
        if isinstance(has_empty_fields, str):
            errors.append(has_empty_fields)

        is_price_valid = price_validation(price=request.POST.get("price"))
        if isinstance(is_price_valid, str):
            errors.append(is_price_valid)

        is_str_valid = string_format_validation(value=request.POST.get("product_type"))
        if isinstance(is_str_valid, str):
            errors.append(is_str_valid)
        is_str_valid = string_format_validation(value=request.POST.get("name"))
        if isinstance(is_str_valid, str):
            errors.append(is_str_valid)

        # if not valid response error
        if len(errors) > 0:
            return JsonResponse(errors, safe=False)
        try:
            q = Products(
                name=request.POST.get("name"),
                product_type=request.POST.get("product_type"),
                price=request.POST.get("price"),
                created=datetime.utcnow().date())
            q.save()
        except Exception as e:
            return HttpResponse(e.__cause__)

        q = ProductChangeLog(
            product_name=request.POST.get("name"),
            price=request.POST.get("price"),
            start_date=datetime.utcnow().date(),
            end_date=app_conf['max_end_date'],
        )
        q.save()

        return redirect('/')


def product_change_price(request):
    if request.method == "POST":

        errors = []

        has_empty_fields = has_empty_fields_validation(required_params=['name', 'price', 'start_date'],
                                                       request=request)
        if isinstance(has_empty_fields, list):
            errors.append(has_empty_fields)

        price = price_validation(price=request.POST.get("price"))
        if isinstance(price, str):
            errors.append(price)

        dates = dates_validation(start=request.POST.get("start_date"), end=request.POST.get("end_date"))
        if isinstance(dates, str):
            errors.append(dates)

        if request.POST.get("end_date") == '':
            end_date = app_conf['max_end_date']
        else:
            end_date = request.POST.get("end_date")

        is_str_valid = string_format_validation(value=request.POST.get("name"))
        if isinstance(is_str_valid, str):
            errors.append(is_str_valid)

        # if not valid response error
        if len(errors) > 0:
            return JsonResponse(errors, safe=False)

        q = ProductChangeLog(
            product_name=request.POST.get("name"),
            price=request.POST.get("price"),
            start_date=request.POST.get("start_date"),
            end_date=end_date,
        )
        q.save()

        return redirect('/')


def group_change_price(request):
    if request.method == "POST":

        errors = []

        has_empty_fields = has_empty_fields_validation(
            required_params=['product_type', 'price', 'start_date'], request=request)

        if isinstance(has_empty_fields, list):
            errors.append(has_empty_fields)

        price = price_validation(price=request.POST.get("price"))
        if isinstance(price, str):
            errors.append(price)

        dates = dates_validation(start=request.POST.get("start_date"), end=request.POST.get("end_date"))
        if isinstance(dates, str):
            errors.append(dates)

        if request.POST.get("end_date") == '':
            end_date = app_conf['max_end_date']
        else:
            end_date = request.POST.get("end_date")

        is_str_valid = string_format_validation(value=request.POST.get("product_type"))
        if isinstance(is_str_valid, str):
            errors.append(is_str_valid)

        # if not valid response error
        if len(errors) > 0:
            return JsonResponse(errors, safe=False)

        q = ProductTypeChangeLog(
            product_type=request.POST.get("product_type"),
            price=request.POST.get("price"),
            start_date=request.POST.get("start_date"),
            end_date=end_date,
        )
        q.save()

        return redirect('/')


def data_by_product_name(request):
    if request.method == "POST":

        errors = []

        has_empty_fields = has_empty_fields_validation(required_params=['name', 'start_date'],
                                                       request=request)

        if isinstance(has_empty_fields, list):
            errors.append(has_empty_fields)

        dates = dates_validation(start=request.POST.get("start_date"), end=request.POST.get("end_date"))
        if isinstance(dates, str):
            errors.append(dates)

        if request.POST.get("end_date") == '':
            end_date = app_conf['max_end_date']
        else:
            end_date = request.POST.get("end_date")

        is_str_valid = string_format_validation(value=request.POST.get("name"))
        if isinstance(is_str_valid, str):
            errors.append(is_str_valid)

        # if not valid response error
        if len(errors) > 0:
            return JsonResponse(errors, safe=False)

        product = Products.objects.all().filter(name=request.POST.get("name"))

        if len(product) == 0:
            return HttpResponse('No such product with this name')

        dates_dict = {}

        product_price_changes = ProductChangeLog.objects.all().filter(product_name=request.POST.get("name"))

        group_price_changes = ProductTypeChangeLog.objects.all().filter(product_type=product[0].product_type)

        all_changes = list(chain(product_price_changes, group_price_changes))
        all_changes_sorted = sorted(all_changes, key=lambda k: k.start_date)

        # fill in date line with data
        for change in all_changes_sorted:
            change_price_date_line(dates_dict=dates_dict, price=change.price,
                                   interval_start_date=change.start_date,
                                   interval_end_date=change.end_date)

        aver_weekly = average_weekly(dates_dict=dates_dict)

        aver_monthly = average_monthly(dates_dict=dates_dict)

        dates_interval = {
            'start_date': request.POST.get("start_date"),
            'end_date': end_date,
        }

        # adda average data
        data_prepared = prepare_data(dates_dict=dates_dict, aver_weekly=aver_weekly, aver_monthly=aver_monthly,
                                     dates_interval=dates_interval)

        return render(request, 'data.html', {'data': json.dumps(data_prepared)})
