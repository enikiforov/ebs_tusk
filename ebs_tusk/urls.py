
from django.contrib import admin
from django.urls import path

from products.views import index, insert_product, product_change_price, group_change_price, data_by_product_name

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('insert_product', insert_product),
    path('product_change_price', product_change_price),
    path('group_change_price', group_change_price),
    path('data_by_product_name', data_by_product_name),
]
